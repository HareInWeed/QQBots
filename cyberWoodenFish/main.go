package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"
	"regexp"
	"syscall"

	"gitlab.com/HareInWeed/QQBots/bot"
	"gitlab.com/HareInWeed/QQBots/bot/intents"
	"gitlab.com/HareInWeed/QQBots/bot/objects"
)

func logError(format string, v ...any) {
	log.Printf("[ERROR] "+format, v...)
}
func logPanic(format string, v ...any) {
	log.Panicf("[FATAL] "+format, v...)
}

func main() {
	lis, cmd, err := bot.New(bot.SandboxEndpoint, token)
	if err != nil {
		logPanic("%v", err)
	}

	err = lis.Connect()
	if err != nil {
		logPanic("%v", err)
	}

	me, err := cmd.GetUser()
	if err != nil {
		logPanic("%v", err)
	}

	err = lis.Identify(
		intents.GUILD_MESSAGES |
			intents.DIRECT_MESSAGE |
			intents.GUILD_MESSAGE_REACTIONS,
	)
	if err != nil {
		logPanic("%v", err)
	}

	c := make(chan os.Signal, 10)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	counts := map[string]int{}

	atMsg, err := regexp.Compile("\u003c@!" + me.Id + "\u003e")
	if err != nil {
		logPanic("%v", err)
	}

	for {
		select {
		case evt := <-lis.Listen():
			log.Println(evt)
			if evt.T == nil {
				continue
			}
			switch *evt.T {
			case "MESSAGE_CREATE":
				var msg objects.Message
				err := json.Unmarshal(*evt.D, &msg)
				if err != nil {
					logError("%v", err)
					continue
				}

				atNum := len(atMsg.FindAllStringIndex(msg.Content, -1))
				if atNum == 0 {
					continue
				}

				userKey := msg.Guild_id + "!" + msg.Channel_id + "!" + msg.Author.Id
				counts[userKey] += atNum
				_, err = cmd.SendMessage(msg.Channel_id, map[string]any{
					"content": fmt.Sprintf("%v 敲了 %v 次木鱼，阿弥陀佛", msg.Author.Username, counts[userKey]),
					"msg_id":  msg.Id,
				}, nil)
				if err != nil {
					logError("%v", err)
					continue
				}
			}

		case <-c:
			log.Println("Shutting down...")
			err := lis.Close()
			if err != nil {
				logError("%v", err)
			}
			os.Exit(0)
		}
	}
}
