package commands

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/HareInWeed/QQBots/bot/common"
	"gitlab.com/HareInWeed/QQBots/bot/objects"
)

type Command struct {
	bot *common.Bot

	conn *http.Client
}

func New(bot *common.Bot) Command {
	connection := &http.Client{}
	return Command{
		bot: bot,

		conn: connection,
	}
}

func (cmd *Command) request(res any, method, url string, body any, msgType *string) error {
	var err error

	reqType := "json"
	if msgType != nil && *msgType == "form" {
		reqType = "form"
	}

	reqBytes := []byte{}
	switch reqType {
	case "form":
		// TODO
	default:
		reqBytes, err = json.Marshal(body)
		if err != nil {
			return err
		}
	}

	req, err := http.NewRequest(method, fmt.Sprintf("%v%v", cmd.bot.Endpoint, url), bytes.NewBuffer(reqBytes))
	if err != nil {
		return err
	}

	var contentType string
	switch reqType {
	case "form":
		contentType = "multipart/form-data"
	default:
		contentType = "application/json; charset=UTF-8"
	}
	req.Header.Add("Content-Type", contentType)
	req.Header.Add("Authorization", cmd.bot.AuthKey)

	resp, err := cmd.conn.Do(req)
	if err != nil {
		return err
	}

	if resp.Body != nil {
		defer resp.Body.Close()
	}

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		return common.HttpError{
			Req:    fmt.Sprintf("%v %v", method, url),
			Status: resp.StatusCode,
			Detail: string(bodyBytes),
		}
	}

	if res != nil {
		err = json.Unmarshal(bodyBytes, res)
		if err != nil {
			return err
		}
	}

	return nil
}

func (cmd *Command) GetWssEndpoint() (string, error) {
	var res any
	err := cmd.request(&res, "GET", "/gateway", nil, nil)
	if err != nil {
		return "", err
	}
	resMap, ok := res.(map[string]any)
	if !ok {
		return "", common.MsgError{Msg: "failed to parse json: ", Detail: res}
	}
	rawUrl, ok := resMap["url"]
	if !ok {
		return "", common.MsgError{Msg: "failed to parse json: ", Detail: resMap}
	}
	url, ok := rawUrl.(string)
	if !ok {
		return "", common.MsgError{Msg: "failed to parse json: ", Detail: rawUrl}
	}
	return url, nil
}

func (cmd *Command) GetUser() (*objects.User, error) {
	var res objects.User
	err := cmd.request(&res, "GET", "/users/@me", nil, nil)
	if err != nil {
		return nil, err
	}
	return &res, nil
}

func (cmd *Command) SendMessage(channel string, msg any, msgType *string) (*objects.Message, error) {
	var res objects.Message
	err := cmd.request(&res, "POST", "/channels/"+channel+"/messages", msg, msgType)
	if err != nil {
		return nil, err
	}
	return &res, nil
}
