package opcode

const Dispatch = 0
const Heartbeat = 1
const Identify = 2
const Resume = 6
const Reconnect = 7
const InvalidSession = 9
const Hello = 10
const HeartbeatACK = 11
const HTTPCallbackACK = 12
