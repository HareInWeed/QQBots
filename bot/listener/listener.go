package listener

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"

	"gitlab.com/HareInWeed/QQBots/bot/commands"
	"gitlab.com/HareInWeed/QQBots/bot/common"
	"gitlab.com/HareInWeed/QQBots/bot/internal"
	"gitlab.com/HareInWeed/QQBots/bot/opcode"
)

const RetryNum = 5

type Listener struct {
	bot *common.Bot

	cmd     *commands.Command
	conn    *websocket.Conn
	evtChan chan common.Event
	ctx     context.Context
	close   context.CancelFunc

	seq        *int
	sessionId  string
	HbInterval float64
	seqLock    sync.RWMutex
}

func New(bot *common.Bot, cmd *commands.Command) Listener {
	return Listener{
		bot:     bot,
		cmd:     cmd,
		evtChan: make(chan common.Event, 100),
	}
}

func (lis *Listener) Connect() error {
	header := http.Header{}
	header.Add("Authorization", lis.bot.AuthKey)
	conn, resp, err := websocket.DefaultDialer.Dial(lis.bot.WssEndpoint, header)
	if err != nil {
		var detail any = nil
		if resp.Body != nil {
			errMsg, err := io.ReadAll(resp.Body)
			if err == nil {
				detail = errMsg
			}
		}
		return common.HttpError{
			Req:    lis.bot.WssEndpoint,
			Status: resp.StatusCode,
			Detail: detail,
		}
	}
	if lis.conn != nil {
		lis.Close()
	}
	lis.conn = conn
	lis.ctx, lis.close = context.WithCancel(context.Background())

	// polling events
	go lis.pollEvents()

	// read hello message
	evt := <-lis.evtChan
	if evt.Op != opcode.Hello {
		defer lis.Close()
		return common.MsgError{Msg: "Didn't receive Hello Message", Detail: evt}
	}
	type Json struct {
		Heartbeat_interval float64
	}
	var helloMsg Json
	err = json.Unmarshal(*evt.D, &helloMsg)
	if err != nil {
		return err
	}
	lis.HbInterval = helloMsg.Heartbeat_interval
	log.Printf("Heartbeat Interval: %vms", lis.HbInterval)

	return nil
}

func (lis *Listener) pollEvents() {
	for {
		var evt common.Event
		err := lis.conn.ReadJSON(&evt)

		if err != nil {
			log.Printf("[ERROR] %v", err)
			return
		}

		if evt.S != nil {
			lis.seqLock.Lock()
			lis.seq = evt.S
			lis.seqLock.Unlock()
		}

		lis.evtChan <- evt
	}
}

func (lis *Listener) KeepAlive() {
	for {
		select {
		case <-lis.ctx.Done():
			return
		case <-time.After(time.Duration(lis.HbInterval * float64(time.Millisecond))):
			lis.seqLock.RLock()
			seq := lis.seq
			lis.seqLock.RUnlock()

			if lis.seq != nil {
				log.Printf("Heartbeat %v", *lis.seq)
			} else {
				log.Println("Heartbeat <nil>")
			}
			err := lis.conn.WriteJSON(map[string]any{
				"op": opcode.Heartbeat,
				"d":  seq,
			})
			if err == nil {
				continue
			}
			internal.LogError("%v", err)
			if !websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				continue
			}
			recovered := false
			for i := 0; !recovered && i < RetryNum; i++ {
				log.Printf("Trying to resume... %v/%v", i, RetryNum)
				err = lis.Close()
				if err != nil {
					internal.LogError("%v", err)
					continue
				}
				err = lis.Connect()
				if err != nil {
					internal.LogError("%v", err)
					continue
				}
				err = lis.Resume()
				if err != nil {
					internal.LogError("%v", err)
					continue
				}
			}
			if !recovered {
				log.Panic("[FATAL] Failed to resume connection")
			}
		}
	}
}

func (lis *Listener) Resume() error {
	lis.seqLock.RLock()
	seq := lis.seq
	lis.seqLock.RUnlock()

	lis.conn.WriteJSON(map[string]any{
		"op": opcode.Identify,
		"d": map[string]any{
			"token":      lis.bot.AuthKey,
			"session_id": []int{0, 1},
			"seq":        *seq,
		},
	})

	return nil
}

func (lis *Listener) Identify(intents int) error {
	err := lis.conn.WriteJSON(map[string]any{
		"op": opcode.Identify,
		"d": map[string]any{
			"token":      lis.bot.AuthKey,
			"intents":    intents,
			"shard":      []int{0, 1},
			"properties": map[string]any{},
		},
	})
	if err != nil {
		return err
	}

	evt := <-lis.evtChan
	if evt.Op != opcode.Dispatch || evt.T == nil || *evt.T != "READY" {
		defer lis.Close()
		return common.MsgError{Msg: "Didn't receive session info", Detail: evt}
	}
	type Json struct {
		Session_id string
	}
	var readyEvt Json
	err = json.Unmarshal(*evt.D, &readyEvt)
	if err != nil {
		return err
	}
	lis.sessionId = readyEvt.Session_id

	go lis.KeepAlive()
	return nil
}

func (lis *Listener) Listen() chan common.Event {
	return lis.evtChan
}

func (lis *Listener) Unlisten(chan common.Event) {
	// TODO
}

func (lis *Listener) Close() error {
	lis.conn.Close()
	lis.close()
	lis.conn = nil
	lis.evtChan = nil
	return nil
}
