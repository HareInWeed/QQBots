package intents

const GUILDS = 1 << 0
const GUILD_MEMBERS = 1 << 1
const GUILD_MESSAGES = 1 << 9
const GUILD_MESSAGE_REACTIONS = 1 << 10
const DIRECT_MESSAGE = 1 << 12
const OPEN_FORUMS_EVENT = 1 << 18
const AUDIO_OR_LIVE_CHANNEL_MEMBER = 1 << 19
const INTERACTION = 1 << 26
const MESSAGE_AUDIT = 1 << 27
const FORUMS_EVENT = 1 << 28
const AUDIO_ACTION = 1 << 29
const PUBLIC_GUILD_MESSAGES = 1 << 30
