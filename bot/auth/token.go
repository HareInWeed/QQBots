package auth

import (
	"fmt"
)

type Token interface {
	AppId() string
	Token() string
}

func AuthKey(token Token) string {
	return fmt.Sprintf("Bot %v.%v", token.AppId(), token.Token())
}
