package bot

import (
	"gitlab.com/HareInWeed/QQBots/bot/auth"
	"gitlab.com/HareInWeed/QQBots/bot/commands"
	"gitlab.com/HareInWeed/QQBots/bot/common"
	"gitlab.com/HareInWeed/QQBots/bot/listener"
)

const DefaultEndpoint = "https://api.sgroup.qq.com"
const SandboxEndpoint = "https://sandbox.api.sgroup.qq.com"

func New(endpoint string, token auth.Token) (*listener.Listener, *commands.Command, error) {
	bot := common.Bot{
		Endpoint: endpoint,
		AuthKey:  auth.AuthKey(token),
	}
	cmd := commands.New(&bot)

	wssEp, err := cmd.GetWssEndpoint()
	if err != nil {
		return nil, nil, err
	}
	bot.WssEndpoint = wssEp

	lis := listener.New(&bot, &cmd)
	return &lis, &cmd, nil
}
