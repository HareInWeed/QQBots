package common

import (
	"bytes"
	"encoding/json"
	"fmt"
)

type Bot struct {
	Endpoint    string
	AuthKey     string
	WssEndpoint string
}

type MsgError struct {
	Msg    string
	Detail any
}

type Event struct {
	Op int
	S  *int
	T  *string
	D  *json.RawMessage
}

func (evt Event) String() string {
	buf := new(bytes.Buffer)
	fmt.Fprintf(buf, "{%v", evt.Op)
	if evt.S != nil {
		fmt.Fprintf(buf, ", %v", *evt.S)
	}
	if evt.T != nil {
		fmt.Fprintf(buf, ", %v", *evt.T)
	}
	if evt.D != nil {
		json, _ := evt.D.MarshalJSON()
		fmt.Fprintf(buf, ", %v", string(json))
	}
	fmt.Fprintf(buf, "}")
	return buf.String()
}

func (err MsgError) Error() string {
	return fmt.Sprintf("%v %v", err.Msg, err.Detail)
}

type HttpError struct {
	Req    string
	Status int
	Detail any
}

func (err HttpError) Error() string {
	return fmt.Sprintf(`"%v" failed (%v): %v`, err.Req, err.Status, err.Detail)
}
