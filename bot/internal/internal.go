package internal

import "log"

func LogError(format string, v ...any) {
	log.Printf("[ERROR] "+format, v...)
}
