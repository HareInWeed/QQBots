package objects

type User struct {
	Id                 string // 用户 id
	Username           string // 用户名
	Avatar             string // 用户头像地址
	Bot                bool   // 是否是机器人
	Union_openid       string // 特殊关联应用的 openid，需要特殊申请并配置后才会返回。如需申请，请联系平台运营人员。
	Union_user_account string // 机器人关联的互联应用的用户信息，与union_openid关联的应用是同一个。如需申请，请联系平台运营人员。
}

type Guild struct {
	Id           string // 频道ID
	Name         string // 频道名称
	Icon         string // 频道头像地址
	Owner_id     string // 创建人用户ID
	Owner        bool   // 当前人是否是创建人
	Member_count int    // 成员数
	Max_members  int    // 最大成员数
	Description  string // 描述
	Joined_at    string // 加入时间
}

type Channel struct {
	Id               string // 子频道 id
	Guild_id         string // 频道 id
	Name             string // 子频道名
	Type             int    // 子频道类型 ChannelType
	Sub_type         int    // 子频道子类型 ChannelSubType
	Position         int    // 排序值，具体请参考 有关 position 的说明
	Parent_id        string // 所属分组 id，仅对子频道有效，对 子频道分组（ChannelType=4） 无效
	Owner_id         string // 创建人 id
	Private_type     int    // 子频道私密类型 PrivateType
	Speak_permission int    // 子频道发言权限 SpeakPermission
	Application_id   string // 用于标识应用子频道应用类型，仅应用子频道时会使用该字段，具体定义请参考 应用子频道的应用类型
	Permissions      string // 用户拥有的子频道权限 Permissions
}

type Member struct {
	User      User     // 用户的频道基础信息，只有成员相关接口中会填充此信息
	Nick      string   // 用户的昵称
	Roles     []string // 用户在频道内的身份组ID, 默认值可参考DefaultRoles
	Joined_at string   // 用户加入频道的时间
}

type MemberWithGuildID struct {
	Guild_id  string   // 频道id
	User      User     // 用户的频道基础信息
	Nick      string   // 用户的昵称
	Roles     []string // 用户在频道内的身份
	Joined_at string   // 用户加入频道的时间
}

type Message struct {
	Id                string              // 消息 id
	Channel_id        string              // 子频道 id
	Guild_id          string              // 频道 id
	Content           string              // 消息内容
	Timestamp         string              // 消息创建时间
	Edited_timestamp  string              // 消息编辑时间
	Mention_everyone  bool                // 是否是@全员消息
	Author            User                // 消息创建者
	Attachments       []MessageAttachment // 附件
	Embeds            []MessageEmbed      // embed
	Mentions          []User              // 消息中@的人
	Member            Member              // 消息创建者的member信息
	Ark               MessageArk          // ark消息
	Seq               int                 // 用于消息间的排序，seq 在同一子频道中按从先到后的顺序递增，不同的子频道之间消息无法排序。(目前只在消息事件中有值，2022年8月1日 后续废弃)
	Seq_in_channel    string              // 子频道消息 seq，用于消息间的排序，seq 在同一子频道中按从先到后的顺序递增，不同的子频道之间消息无法排序
	Message_reference MessageReference    // 引用消息对象
	Src_guild_id      string              // 用于私信场景下识别真实的来源频道 id
}

type MessageAttachment any // TODO
type MessageEmbed any      // TODO
type MessageArk any        // TODO
type MessageReference any  // TODO
