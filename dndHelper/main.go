package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"regexp"
	"strconv"
	"syscall"

	"gitlab.com/HareInWeed/QQBots/bot"
	"gitlab.com/HareInWeed/QQBots/bot/intents"
	"gitlab.com/HareInWeed/QQBots/bot/objects"
)

func logError(format string, v ...any) {
	log.Printf("[ERROR] "+format, v...)
}
func logPanic(format string, v ...any) {
	log.Panicf("[FATAL] "+format, v...)
}

func main() {
	lis, cmd, err := bot.New(bot.SandboxEndpoint, token)
	if err != nil {
		logPanic("%v", err)
	}

	err = lis.Connect()
	if err != nil {
		logPanic("%v", err)
	}

	me, err := cmd.GetUser()
	if err != nil {
		logPanic("%v", err)
	}

	err = lis.Identify(
		intents.GUILD_MESSAGES |
			intents.DIRECT_MESSAGE |
			intents.GUILD_MESSAGE_REACTIONS,
	)
	if err != nil {
		logPanic("%v", err)
	}

	c := make(chan os.Signal, 10)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	atMsgRegex, err := regexp.Compile("\u003c@!" + me.Id + "\u003e")
	if err != nil {
		logPanic("%v", err)
	}

	diceExp, err := regexp.Compile(`(\d+)d(\d+)`)
	if err != nil {
		logPanic("%v", err)
	}

	for {
		select {
		case evt := <-lis.Listen():
			log.Println(evt)
			if evt.T == nil {
				continue
			}
			switch *evt.T {
			case "MESSAGE_CREATE":
				var msg objects.Message
				err := json.Unmarshal(*evt.D, &msg)
				if err != nil {
					logError("%v", err)
					continue
				}

				if !atMsgRegex.MatchString(msg.Content) {
					continue
				}

				matches := diceExp.FindAllStringSubmatch(msg.Content, -1)
				buf := new(bytes.Buffer)
				fmt.Fprintf(buf, "验定结果：\n")
				count := 0
				sum := 0
				for i, v := range matches {
					times, _ := strconv.Atoi(v[1])
					faces, _ := strconv.Atoi(v[2])
					for j := 0; j < times; j++ {
						count++
						res := rand.Intn(faces) + 1
						sum += res
						fmt.Fprintf(buf, "%v", res)
						if i != len(matches)-1 || j != times-1 {
							fmt.Fprintf(buf, "+")
						} else if count > 1 {
							fmt.Fprintf(buf, "=")
						}
					}
					if len(matches) > 1 {
						fmt.Fprintf(buf, "\n")
					}
				}
				if count > 1 {
					fmt.Fprintf(buf, "%v", sum)
				}

				_, err = cmd.SendMessage(msg.Channel_id, map[string]any{
					"content": buf.String(),
					"msg_id":  msg.Id,
				}, nil)
				if err != nil {
					logError("%v", err)
					continue
				}
			}

		case <-c:
			log.Println("Shutting down...")
			err := lis.Close()
			if err != nil {
				logError("%v", err)
			}
			os.Exit(0)
		}
	}
}
